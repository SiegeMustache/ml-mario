﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BrainUnit
{
    public List<Brain.Movement> movements;
    public float fitness;

    public BrainUnit(List<Brain.Movement> list, float fitnessValue)
    {
        movements = list;
        fitness = fitnessValue;
    }
    
    public BrainUnit()
    {
        movements = new List<Brain.Movement>();
        fitness = 0;
    }
}
