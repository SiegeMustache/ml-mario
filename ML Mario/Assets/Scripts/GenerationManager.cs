﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerationManager : Singleton<GenerationManager>
{
    public int generations;
    public int currentGeneration;
    public int population;
    public int numberOfCommands;

    public float mutationChance;
    float originalMutationChance;
 

    public float unitMoveSpeed;
    public float unitJumpForce;
    public float commandDuration;

    public float deathFitnessPenalty;
    public float goalFitnessBonus;

    public GameObject startPosition;
    [HideInInspector]
    public int currentCommandIndex;

    public List<Brain> currentPopulation = new List<Brain>();

    public float eliteFitness;
    public BrainUnit eliteBrainUnit = new BrainUnit();

    float commandTimer;
    bool allEnded = false;

    // Start is called before the first frame update
    void Start()
    {
        originalMutationChance = mutationChance;
        commandTimer = commandDuration;
        currentCommandIndex = 0;
        Generator.instance.SpawnGeneration();
    }

    // Update is called once per frame
    void Update()
    {
        if(currentGeneration < generations)
        {
            if (commandTimer > 0 &&
                currentCommandIndex < numberOfCommands)
            {
                commandTimer -= Time.deltaTime;
                allEnded = true;
                foreach(Brain brain in currentPopulation)
                {
                    if (!brain.hasEnded)
                        allEnded = false;
                }
                if(allEnded)
                    commandTimer = 0;

            }
            else if (commandTimer <= 0 && currentCommandIndex < numberOfCommands)
            {
                currentCommandIndex++;
                if (currentCommandIndex < numberOfCommands && !allEnded)
                {
                    commandTimer = commandDuration;
                }
                else if(currentCommandIndex >= numberOfCommands || allEnded)
                {
                    SetupNextGeneration();
                }
            }
        }
    }

    void GatherEliteBrain(List<Brain> brains)
    {
        Brain bestBrain = new Brain();

        foreach(Brain brain in brains)
        {
            if(!bestBrain)
                bestBrain = brain;
            else
            {
                if (bestBrain.fitness < brain.fitness)
                    bestBrain = brain;
            }
        }

        if (bestBrain.hasWon)
            mutationChance /= 2;
        else
            mutationChance = originalMutationChance;

        eliteBrainUnit.fitness = bestBrain.fitness;
        eliteBrainUnit.movements = bestBrain.movementCommands;
    }

    void ClearCurrentPopulation(List<Brain> brains)
    {
        foreach(Brain brain in brains)
        {
            brain.ResetBrain();
        }
    }

    void SetupNextGeneration()
    {
        GatherEliteBrain(currentPopulation);
        ClearCurrentPopulation(currentPopulation);
        currentGeneration++;
        commandTimer = commandDuration;
        currentCommandIndex = 0;
    }
}
