﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : Singleton<Generator>
{
    public GameObject unitPrefab;

    public void SpawnGeneration()
    {
        for(int i = 0; i < GenerationManager.instance.population; i++)
        {
            GameObject go = Instantiate(unitPrefab, GenerationManager.instance.startPosition.transform.position, Quaternion.identity);
            GenerationManager.instance.currentPopulation.Add(go.GetComponent<Brain>());
        }
    }
}
