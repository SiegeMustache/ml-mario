﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brain : MonoBehaviour
{
    public bool hasEnded = false;
    public bool hasWon = false;

    public float fitness;

    public Vector3 startPosition;

    Transform thisTransform;
    Mover mover;

    public enum Movement { GoRight, GoLeft, JumpToRight, JumpToLeft };
    public List<Movement> movementCommands = new List<Movement>();

    // Start is called before the first frame update
    void Start()
    {
        thisTransform = transform;
        mover = GetComponent<Mover>();
        startPosition = GenerationManager.instance.startPosition.transform.position;
        thisTransform.position = startPosition;
        fitness = 0;

        if(movementCommands.Count == 0)
        {
            GenerateComandList();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasEnded && GenerationManager.instance.currentCommandIndex < movementCommands.Count)
        {
            ExecuteCommand(movementCommands[GenerationManager.instance.currentCommandIndex]);
            EvaluateFitness();
        }
    }


    public void EvaluateFitness()
    {
        fitness = Vector3.Distance(thisTransform.position, startPosition);
    }

    public void InitializeSubject(List<Movement> listOfCommands)
    {
        for(int i = 0; i < listOfCommands.Count; i++)
        {
            movementCommands[i] = listOfCommands[i];
        }
    }

    void ExecuteCommand(Movement typeOfMovement)
    {
        switch(typeOfMovement)
        {
            case Movement.GoRight:
                {
                    mover.GoRight();
                    break;
                }
            case Movement.GoLeft:
                {
                    mover.GoLeft();
                    break;
                }
            case Movement.JumpToRight:
                {
                    mover.JumpToRight();
                    break;
                }
            case Movement.JumpToLeft:
                {
                    mover.JumpToLeft();
                    break;
                }
        }
    }

    public void GenerateComandList()
    {
        for(int i = 0; i < GenerationManager.instance.numberOfCommands; i++)
        {
            movementCommands.Add(GenerateMovementCommand());
        }
    }

    Movement GenerateMovementCommand()
    {
        int randomInt = Random.Range(0, 4);
        
        switch(randomInt)
        {
            case (0):
                {
                    return Movement.GoRight;
                }
            case (1):
                {
                    return Movement.GoLeft;
                }
            case (2):
                {
                    return Movement.JumpToRight;
                }
            case (3):
                {
                    return Movement.JumpToLeft;
                }
        }
        return Movement.GoRight;
    }

    List<Movement> MutateCommands(List<Movement> commands)
    {
        for(int i = 0; i < commands.Count; i++)
        {
            int randomInt = Random.Range(0, 100);
            if (randomInt <= GenerationManager.instance.mutationChance)
                commands[i] = GenerateMovementCommand();
        }

        return commands;
    }

    public void ResetBrain()
    {
        hasEnded = false;
        hasWon = false;
        thisTransform.position = startPosition;
        fitness = 0;
        GetComponent<MeshRenderer>().material.color = Color.white;
        GetComponent<Rigidbody>().isKinematic = false;
        InitializeSubject(GenerationManager.instance.eliteBrainUnit.movements);
        movementCommands = MutateCommands(movementCommands);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("DeathWall"))
        {
            if (!hasEnded)
            {
                hasEnded = true;
                hasWon = false;
                GetComponent<MeshRenderer>().material.color = Color.black;
                GetComponent<Rigidbody>().isKinematic = true;
                EvaluateFitness();
                fitness -= GenerationManager.instance.deathFitnessPenalty;
            }
        }
        else if(collision.collider.gameObject.layer == LayerMask.NameToLayer("Goal"))
        {
            hasEnded = true;
            hasWon = true;
            GetComponent<MeshRenderer>().material.color = Color.yellow;
            GetComponent<Rigidbody>().isKinematic = true;
            EvaluateFitness();
            fitness += GenerationManager.instance.goalFitnessBonus;
        }
    }
}
