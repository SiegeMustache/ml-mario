﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    float moveSpeed;
    float jumpForce;
    public bool isGrounded;
    Rigidbody rb;
    Transform thisTransform;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        thisTransform = transform;
        moveSpeed = GenerationManager.instance.unitMoveSpeed;
        jumpForce = GenerationManager.instance.unitJumpForce;
    }

    private void Update()
    {
        isGrounded = CheckGround();
    }

    public void GoRight()
    {
        rb.MovePosition(thisTransform.position + thisTransform.right * Time.deltaTime * moveSpeed);
    }

    public void GoLeft()
    {
        rb.MovePosition(thisTransform.position + (-thisTransform.right) * Time.deltaTime * moveSpeed);
    }

    void JumpOnSpot()
    {
        if (CheckGround())
            rb.AddForce(thisTransform.up * jumpForce, ForceMode.Impulse);
    }

    public void JumpToRight()
    {
        if (CheckGround())
            JumpOnSpot();

        GoRight();
    }

    public void JumpToLeft()
    {
        if(CheckGround())
            JumpOnSpot();

        GoLeft();
    }

    bool CheckGround()
    {
        if(Physics.Raycast(thisTransform.position, -thisTransform.up, out RaycastHit hit, 0.7f, 1 << LayerMask.NameToLayer("Ground")))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
