﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text currentGen;
    public Text bestFitness;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GenerationManager.instance.eliteBrainUnit.fitness != 0)
            bestFitness.text = "Best Fitness: " +  GenerationManager.instance.eliteBrainUnit.fitness.ToString();
        else
            bestFitness.text = "Best Fitness: N/A";

        currentGen.text = "Current Gen: " + GenerationManager.instance.currentGeneration.ToString();
    }
}
